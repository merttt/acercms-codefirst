﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AcerCms.UI.Areas.Admin.Models
{
    public class PictureViewModel
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public string Path { get; set; }
        public string FileName { get; set; }
        public bool IsSlider { get; set; }        

        public virtual ICollection<NewsViewModel> News { get; set; }
    }
}