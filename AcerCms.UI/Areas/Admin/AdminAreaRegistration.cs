﻿using System.Web.Mvc;

namespace AcerCms.UI.Areas.Admin
{
    public class AdminAreaRegistration : AreaRegistration 
    {
        public override string AreaName 
        {
            get 
            {
                return "Admin";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context) 
        {
            context.MapRoute(
                "Admin_default",
                "Admin/{controller}/{action}/{id}",
                defaults: new { controller = "Home", action = "Index", area = "Admin", id = UrlParameter.Optional },
                namespaces: new[] { "AcerCms.UI.Areas.Admin.Controllers" }
            );
        }
    }
}