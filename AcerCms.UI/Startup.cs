﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(AcerCms.UI.Startup))]
namespace AcerCms.UI
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
