using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace AcerCms.Dal.Enities
{
    public class Picture
    {      
        public int Id { get; set; }                
        public string Title { get; set; }
        public string Description { get; set; }
        public string Path { get; set; }
        public string FileName { get; set; }
        public bool IsSlider { get; set; }
        public bool IsDeleted { get; set; }
    
        public virtual ICollection<News> News { get; set; }
    }
}