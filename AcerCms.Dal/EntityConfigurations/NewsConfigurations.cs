﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AcerCms.Dal.Enities;

namespace AcerCms.Dal.EntityConfigurations
{
    public class NewsConfigurations : EntityTypeConfiguration<News>
    {
        public NewsConfigurations()
        {
            //Anahtar Tanımalaması
            //this.HasKey(k => k.Id);
            //this.Property(p => p.Id).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);

            //Çoklu Anahtar
            //this.HasKey(k => new {k.Id, k.Title});

            this.Property(p => p.Title).HasMaxLength(200).IsRequired();
            this.Property(p => p.ShortDescription).IsOptional().HasMaxLength(400);
            this.Property(p => p.Content).IsMaxLength().IsRequired();
            //this.Property(p => p.UpdateDate).HasColumnType("datetime2");



            //Relation
            this.HasOptional(l=> l.Picture).WithMany(r=> r.News)
                .WillCascadeOnDelete(false);
        }
    }
}
