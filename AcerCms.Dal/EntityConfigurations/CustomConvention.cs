﻿using System;
using System.Data.Entity.ModelConfiguration.Conventions;

namespace AcerCms.Dal.EntityConfigurations
{
    public class CustomConvention : Convention
    {
        public CustomConvention()
        {
            this.Properties<DateTime>().Configure(x=> x.HasColumnType("datetime2"));
            //this.Types().Configure(c => c.ToTable("tbl" + c.GetType().Name));
        }
    }
}
