﻿using System.Data.Entity.ModelConfiguration;
using AcerCms.Dal.Enities;

namespace AcerCms.Dal.EntityConfigurations
{
    public class PictureConfigurations : EntityTypeConfiguration<Picture>
    {
        public PictureConfigurations()
        {            
            this.Property(p => p.Title).HasMaxLength(200).IsRequired();
            this.Property(p => p.Description).HasMaxLength(400).IsOptional();
            this.Property(p => p.FileName).HasMaxLength(256).IsRequired();
            this.Property(p => p.Path).HasMaxLength(400).IsRequired();
        }
    }
}
